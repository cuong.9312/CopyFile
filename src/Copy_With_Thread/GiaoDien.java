package Copy_With_Thread;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.*;
public class GiaoDien extends JFrame implements ActionListener{
	
	JLabel label1= new JLabel("Source: ");
	JLabel label2= new JLabel("Information: ");
	JLabel label3= new JLabel("State: ");
	JTextField text1= new JTextField(30);
	JTextField text2= new JTextField(30);
	JButton button1= new JButton("Open");
	JButton button2= new JButton("Copy");
	JButton button3= new JButton("Exit");
	JFileChooser filechooser= new JFileChooser();
	JProgressBar progressbar= new JProgressBar();
	
	File fileSource;
	File fileDes;
	String extension;
	int buf=1*1024*1024;
	byte [] b1; int le1;
	byte [] b2; int le2;
	byte [] b3; int le3;
	byte [] b4; int le4;
	byte [] b5; int le5;
	BufferedOutputStream bos;
	long sizeFile;
	static long offset=0;
        JLabel label= new JLabel("time main: ");
	
	public GiaoDien() {
		init();
	}
	
	public void init() {
		try{
			BufferedImage img = ImageIO.read(new File("src\\copy_with_thread\\icon.png"));
	        this.setIconImage(img);
		}catch(Exception ex){
			JOptionPane.showMessageDialog(null, ex);
		}
		this.setTitle("Thread_Copy");
		this.setBounds(50, 50, 400, 150);
		this.setLayout(new FlowLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		label1.setPreferredSize(label2.getPreferredSize());
		label3.setPreferredSize(label2.getPreferredSize());
		button3.setPreferredSize(button1.getPreferredSize());
		this.add(label1);
		this.add(text1);
		this.add(button1);
		this.add(label2);
		this.add(text2);
		this.add(button2);
		this.add(label3);
		this.add(progressbar);
		this.add(button3);
                this.add(label);
		progressbar.setPreferredSize(text1.getPreferredSize());
		label.setPreferredSize(text1.getPreferredSize());
		button1.addActionListener(this);
		button2.addActionListener(this);
		button3.addActionListener(this);
		fileSource=new File("");
		fileDes=new File("");
		extension="";
		progressbar.setStringPainted(true);
		
	}
	
	public static void main(String[] args) {
		try{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}catch(Exception ex){
			JOptionPane.showMessageDialog(null, ex);
		}
		new GiaoDien().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton a = (JButton) e.getSource();
		//-------------------------open file-----------------------------
		if(a==button1){
	        if (a == button1) {
	            try {
                        label.setText("time main: ");
	            	offset=0;
	                int select = filechooser.showOpenDialog(null);
	                if (select == JFileChooser.OPEN_DIALOG) {
	                    fileSource = filechooser.getSelectedFile();
	                    String name = fileSource.getName();
	                    int i = name.lastIndexOf(".");
	                    if(i==-1){
	                    	extension="";
	 	                    text1.setText(fileSource.getPath());
	 	                   long size = (long) fileSource.length() / 1024 / 1024;
	 	                    text2.setText(size + " Mb   , type: " + null);

	 	                    if (size == 0) {
	 	                        size = (long) fileSource.length() / 1024;
	 	                        text2.setText(size + " Kb   , type: " + null);
	 	                    }
	 	                    progressbar.setValue(0);
	                    }
	                    else{
	                    	 extension = name.substring(i, name.length());
	 	                    text1.setText(fileSource.getPath());
	 	                   long size = (long) fileSource.length() / 1024 / 1024;
	 	                    text2.setText(size + " Mb   , type: " + extension);

	 	                    if (size == 0) {
	 	                        size = (long) fileSource.length() / 1024;
	 	                        text2.setText(size + " Kb   , type: " + extension);
	 	                    }
	 	                   progressbar.setValue(0);
	                    }
	                   
	                }
	            } catch (Exception ex) {
	                JOptionPane.showMessageDialog(null, ex);
	            }
	        }
		}
		//-------------------------copy file-----------------------------
		if(a==button2){
	            try {
	                if (fileSource == null) {
	                    JOptionPane.showMessageDialog(null, "file copy null");
	                } else {
	                    int se = filechooser.showSaveDialog(null);
	                    if (se == JFileChooser.CANCEL_OPTION) {
	                    } else {
	                        new Thread(new Runnable() {
	                            @Override
	                            public void run() {
	                                try{
	                                	Date d1= new Date();
		                                File filecopy1 = filechooser.getSelectedFile();
		                                if(filecopy1.getPath().toUpperCase().endsWith(extension.toUpperCase())==false)
		                                fileDes = new File(filecopy1.getPath() + "" + extension);
		                                else fileDes= filecopy1;
		                                bos = new BufferedOutputStream(new FileOutputStream(fileDes));
	                                	
		                    			while(true){
		                    				Thread t1= new Thread(new Runnable() {
		                    					
		                    					@Override
		                    					public void run() {
		                    						try{
		                    							BufferedInputStream	bis= new BufferedInputStream(new FileInputStream(fileSource));
		                    							bis.skip(offset);
		                    							b1= new byte[buf];
		                    							le1=bis.read(b1);
		                    							bis.close();
		                    						}catch(Exception ex){
		                    						}
		                    					}
		                    				});
		                    				Thread t2= new Thread(new Runnable() {
		                    					
		                    					@Override
		                    					public void run() {
		                    						try{
		                    							BufferedInputStream bis= new BufferedInputStream(new FileInputStream(fileSource));
		                    							bis.skip(offset+buf);
		                    							b2= new byte[buf];
		                    							le2=bis.read(b2);
		                    							bis.close();
		                    						}catch(Exception ex){
		                    						}      					}
		                    				});
		                    				Thread t3= new Thread(new Runnable() {
		                    					
		                    					@Override
		                    					public void run() {
		                    						try{
		                    							BufferedInputStream bis= new BufferedInputStream(new FileInputStream(fileSource));
		                    							bis.skip(offset+buf*2);
		                    							b3= new byte[buf];
		                    							le3=bis.read(b3);
		                    							bis.close();
		                    						}catch(Exception ex){
		                    						}
		                    					}
		                    				});
		                    				Thread t4= new Thread(new Runnable() {
		                    					
		                    					@Override
		                    					public void run() {
		                    						try{
		                    							BufferedInputStream bis= new BufferedInputStream(new FileInputStream(fileSource));
		                    							bis.skip(offset+buf*3);
		                    							b4= new byte[buf];
		                    							le4=bis.read(b4);
		                    							bis.close();
		                    						}catch(Exception ex){
		                    						}
		                    					}
		                    				});
		                    				Thread t5= new Thread(new Runnable() {
		                    					
		                    					@Override
		                    					public void run() {
		                    						try{
		                    							BufferedInputStream bis= new BufferedInputStream(new FileInputStream(fileSource));
		                    							bis.skip(offset+buf*4);
		                    							b5= new byte[buf];
		                    							le5=bis.read(b5);
		                    							bis.close();
		                    						}catch(Exception ex){
		                    						}
		                    					}
		                    				});
		                    				t1.start();
		                    				t2.start();
		                    				t3.start();
		                    				t4.start();
		                    				t5.start();
		                    				try{
		                    					t1.join();
		                    					t2.join();
		                    					t3.join();
		                    					t4.join();
		                    					t5.join();
		                    					if(le1!=-1){
		                    						bos.write(b1, 0, le1);
		                    						progressbar.setValue((int)((fileDes.length()*100)/fileSource.length()));
		                    					}else{
		                    						break;
		                    					}
		                    					if(le2!=-1){
		                    						bos.write(b2, 0, le2);
		                    						progressbar.setValue((int)((fileDes.length()*100)/fileSource.length()));
		                    					}else{
		                    						break;
		                    					}
		                    					if(le3!=-1){
		                    						bos.write(b3, 0, le3);
		                    						progressbar.setValue((int)((fileDes.length()*100)/fileSource.length()));
		                    					}else{
		                    						break;
		                    					}
		                    					if(le4!=-1){
		                    						bos.write(b4, 0, le4);
		                    						progressbar.setValue((int)((fileDes.length()*100)/fileSource.length()));
		                    					}else{
		                    						break;
		                    					}
		                    					if(le5!=-1){
		                    						bos.write(b5, 0, le5);
		                    						progressbar.setValue((int)((fileDes.length()*100)/fileSource.length()));
		                    					}else{
		                    						break;
		                    					}
		                    					
		                    					if(offset>fileSource.length())break;
		                    					else 
		                    						offset+=5*buf;
		                    					
		                    										
		                    				}catch(Exception ex){
		                    					
		                    				}
		                    			}
		                    			bos.close();
		                    			Date d2= new Date();
                                                        System.out.println((d2.getTime()-d1.getTime())/1000);
                                                        label.setText("time main: "+(d2.getTime()-d1.getTime())/1000);
		                                int sl = JOptionPane.showConfirmDialog(null, "Complete! \n open folder? ", "Open file", JOptionPane.YES_NO_OPTION);
		                                if (sl == JOptionPane.YES_OPTION) {
		                                    Desktop.getDesktop().open(fileDes.getParentFile());
		                                }
	                                }catch(Exception ex){
	                                }

	                            }
	                        }).start();
	                    }
	                }

	            } catch (Exception ex) {
	                JOptionPane.showMessageDialog(null, ex);
	            }
	        }
		//-------------------------exit-----------------------------
		if(a==button3){
			System.exit(0);
		}
		
	}

}
